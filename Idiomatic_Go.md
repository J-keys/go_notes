

## Idiomatic Go


	1) Use Consistent spelling of certain words
		G.EX
			// marshaling
			// unmarshaling
	
	2) Single spaces between sentences
		G.EX
			// Sentence one. Sentence two.

	3) Error variable naming
		G.EX
			// Package level exported error.
			var ErrSomething = errors.New("something went wrong")
			
			func main() {
				// Normally you call it just "err", 
				result, err := doSomething() 
				// and use err right away

				// but if you want to give it a longer name, use "somethingError".
				var specificError error
				result, specificError = doSpecificThing()

				// . . . use specificError later. 
			}

	4) For brands or words with more than one capital letter, lowercase all letters
		G.EX
			// Exported. 
			var OAuthEnabled bool
			var GitHubToken string

			// Unexported 
			var oauthEnabled bool
			var githubToken string

	5) Comments for humans always have a single space after the slashs
		G.EX
			// This is a comment
			// for humans. 

	6) Use defer if its overhead is relatively negligible, or it buys meaningful readability
		Comment 
			Don't use defer if the readability stays the same, as is the case in short 1-2 exit funcs. 
			Using it has a fixed performace overhead, which is noticeable for operations that run on
			the order of nanoseconds. 

	7) Use singular form for collection repo/folder name
		G.EX
			// example is singular form 
			github.com/golang/example/hello
			github.com/golang/example/outyet
			github.com/golang/example/basic

	8) Avoid unused method receiver names
		G.EX
			// use (foo) NOT (f foo) <- unused receiver 
			func (foo) method() {
				. . . 
			}

	9) Empty string check
		G.EX 
			// Checking for empty strings
			if s == "" {
				. . . 
			}

	10) Mutex hat 
		G.EX-1
			struct {
				. . . 

				// rateMu protects rateLimits and mostRecent
				rateMu			sync.Mutex
				rateLimits		[categories]Rate
				mostRecent		rateLimitCategory
			} 
		G.EX-2: When adding a new, unrelated field that isn't protected by rateMu, do this:
			struct { 
				. . . 

				rateMu			sync.Mutex
				rateLimits		[categories]Rate
				mostRecent		rateLimitCategory
			 +
			 +	common service // use both (+) and not a single infront of (common service)
			}

	11) Don't os.Exit(2) inside flag.Usage 
		G.EX
			flag.Usage = func() {
				fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
				flag.PrintDefaults()
				// (UNNEEDED)-> os.Exit(2) <- flag package is already responsible for exiting
			}
				







