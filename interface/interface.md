#### Interfaces _______________________________________________________________________

	- An interface is a set of methods that we use to define a set of actions 
	  The combination of methods is called an interface and can implemented by duelly

	EX.1 Implementing the Interface 

		type Human struct {
			name string
			age int
			phone string
		}

		type Student struct {
			Human 
			school string 
			loan float32
		}

		type Employee struct {
			Human 
			company string
			money float32
		}

		// define interfaces 
		type Men interface {
			SayHi()
			Sing(lyrics string)
			Guzzle(beerStein string)
		}

		type YoungChap interface {
			SayHi()
			Sing(song string)
			BorrowMoney(amount float32)
		}

		type ElderlyGen interface {
			SayHi()
			Sing(song string)
			SpendSalary(amount float32)
		}

		func (h *Human) SayHi(){
			fmt.Printf("Hi, I am %s you can call me on %s\n", h.name, h.phone)
		}

		func (h *Human) Sing(lyrics string) {
			fmt.Println("la la la la la la la. . . ", lyrics)
		}

		func (h *Human) Guzzle(beerStein string) {
			fmt.Println("Guzzle Guzzle Guzzle", beerStein)
		}

		// Employee overloads SayHi
		func (e *Employee) SayHi() {
			fmt.Printf("Hi, I am %s, I work at %s. Call me on %s \n", e.name, 
				e.company, e.phone) 
		}

		func (s *Student) BorrowMoney(amount float32) {
			s.loan += amount // again and again
		}

		func (e *Employee) SpendSalary(amount float32) {
			e.money -= amount // Spending salary 
		}


*** Empty Interfaces **

	An empty interface is a ninterface that doesn't contain any methods, so all types 
	implement an empty interface. 

	EX.2

		// define a as empty interface 
		var void interface{}

		// vars 
		i := 5
		s := "Hello World"

		// a can store value of any type
		void = i 
		void = s 

	EX.2-A
		- If a function uses an empty interface as it argument type, it can accept any type; 
		  if a function uses empty interface as its return value type, it can return any type. 

*** Method Arguments of an Interface

	Any vaiable can be used in an interface

	EX.3 

		package main 

		import (
			"fmt"
			"strconv"
		)

		type Human struct {
			name string
			age int
			phone string 
		}

		// human implememnts fmt.Stringer 
		fucn (h Human) String() string {
			return "Name:" + h.name + ", Age:" + strconv.Itoa(h.age) + "years, Contact:" + h.phone
		}

		func main() {
			Bob := Human{"Bob", 30, "111-111-1111"}
			fmt.Println("this Human is : ", Bob)
		}

**** Type of Variable in an interface

	Go Lang Type Checking: 	Assertion of Comma-ok pattern 

		value, ok := element.(T) 

	This checks to see if the variable is the type that we expect, where <value> is the value of the
	variable, <ok> is a variable of boolean type, <element> is the interface variable and the <T>
	is the type of assertion. 

	EX.4 

		package main 

		import (
			"fmt"
			"strconv"
		)

		type Element interface{}
		type List []Element 

		type Person struct {
			name string 
			age int
		}

		func (p Person) String() string {
			return "(name: " + p.name + " - age: " + strconv.Itoa(p.age) + " years)"
		}

		func main() {
			list := make(List, 3)
			list[0] = 1
			list[1] = "Hello" 
			list[2] = Person{"Dennis", 70}

			for index, element := range list {
				switch value := element.(type) {
				case int: 
					fmt.Printf("list[%d] is an int and its value is %d\n", index, value)
				case string: 
					fmt.Printf("list[%d] is a string and its value is %s\n", index, value)
				case Person:
					fmt.Printf("list[%d] is a person and its value is %s\n", index, value)
				default: 
					fmt.Printf("list[%d] is of a different type\n", index)
				}
			}
		}

	EX.4-A 
		- <element.(type) cannont be used outside of the <switch> body, which means in that 
		  case you have to use the <comma-ok> pattern. 

*** Embedded interfaces 

	
	You can use interfaces as anonymous fields as well, but we call them <Embedded Interfaces>. 
	Here, we follow the same rules as anonymous fields. If an interface has another interface
	embedded within it, it will behave as if it has all the methods that the embedded interface
	has. 

	EX.5 Example Embedded Interface Syntax 

		type Interface interface {
			sort.Interface 			// embedded sort.Interface
			Push(x interface{})		// a Push mehtod to push elements into the heap
			Pop() interface{} 		// a Pop method that pops elements from the heap
		}

	EX.6 Embedded Interface

		type Interface interface {
			// Len is the number of elents in the collection
			Len() int
			// Less returns whether the elements with index i should sort
			// before the element with index j
			Less(i, j int) bool
			// Swap swaps the elements with indexes i and j
			Swap(i, j int)
		}

	EX.7 io.ReadWriter Embedded interface

		// io.ReadWriter
		type ReadWriter interface {
			Reader
			Writer
		}

*** Reflection 

	Reflection in Go is used for determining information at runtime, we use the <reflect> package
	There are three steps involved when using reflect. First we need to convert an interface to
	reflect types (reflect.Type or reflect.Value, this depends on the situation) 

	EX.8 Reflection Syntax 

		t := reflect.TypeOf(i)		// get meta-data in type i, and use t to get all elements
		v := reflect.ValueOf(i) 	// get actual value in type i, and use v to change its value

	EX.9 Convert reflected types to get the values needed

		var x float64 = 3.4

		t := reflect.Typeof(x)
		v := reflect.ValueOf(x)

		fmt.Println("type:", t)
		fmt.Println("value:", v)
		fmt.Println("kind is flaot64:", v.Kind() == reflect.Float64)

	EX.10 Changing values from reflect types

		var x float64 = 3.4
		p := reflect.ValueOf(&x) 	// passed pointer
		v := p.Elem()
		v.SetFloat(7.1) 













































