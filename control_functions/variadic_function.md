
#### Variadic Functions ___________________________________________________________________________

	- Go supports functions with a variable number of arguments. Called variadic functions, which
	  means the function allows an uncertain number of arguments. 

	EX.1

		func myFunc(arg ...int) {} 

	EX.1-A 
		- <arg ...int> tells go that this function has variable arguments. Notice that these 
		  arguments are type <int>. In the body of the function, the <arg> becomes a <slice>
		  of <int>. 

	EX.2 
		for _, n := range arg {
			fmt.Printf("And the number is: %d\n", n)
		} 

	Pass by Value and Pointers: 
		- When you pass an argument to the function that was called, that function actually 
		  gets the copy of your variables so any change will not affect to the original 
		  variable. 

	EX.3 Pass by Value and Pointer

		package main 

		import "fmt"

		// simple function to add 1 to a
		func add1(a int) int {
			a = a + 1 				// we chage value of a
			return a  				// return new value of a
		} 

		func main() {
			x := 3 

			fmt.Printf("x = ", x) 	// should print "x = 3"

			x1 := add1(x) 			// call add1(x) func 

			fmt.Println("x + 1 = ", x1) 	// should print "x+1 = 4"
			fmt.Println("x = ", x) 			// should print "x = 3"
		} 

	EX.3-A 
		- Even though <add1> with <x> was called the origin value of <x> doesn't change. 
		- The reason is simple: when you call <add1>, we game a copy of <x> to it, not
		  the <x> itself. 
	
	- Variables are stored in memory and they have some memory addresses. So if we want to
	  change the value of a variable, we must change its memory address. 
	- Therefore the function <add1> has to know the memory address of <x> in order to change
	  its value. 

	EX.4 Pass pointer copy without coping the value

		package main 

		import "fmt"

		// simple function to add 1 to a
		func add1(a *int) int {		// passing <&x> with pointer type <*int> 
			*a = *a + 1 	// we changed value of a
			return *a		// return new value of a
		}

		func main() {

			x := 3 

			fmt.Println("x = ", x) 		// should print "x = 3"

			x1 := add1(&x) 				// call <add1(&x) to pass the memory address of x

			fmt.Println("x + 1 = ", x1) // should print "x + 1 = 4"
			fmt.Println("x = ", x) 		// should print "x = 4" 
		} 

	EX.4-A	Why do we use pointers? 
		- Allows us to use more functions to operate on one variable 
		- low cost by passing memory address (8 bytes), copy is not an efficient way, both in 
		  terms of time and space, to pass variables. 
		- <channel>, <slice>, and <map> are reference types, so they use pointers when passing
		  to functions by default. (ATTENTION: if you need to change the length of <slice>, you
		  have to pass pointers explicitly) 
		