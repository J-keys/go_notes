#### Web Working Principles _______________________________________________________________

	- We can divide the web's working principles into the following steps: 
		1) Client uses TCP/IP protocol to connect to server
		2) Client sends HTTP request packages to server
		3) Server returns HTTP response packges to client. If the request resources include
		   dynamic scripts, server calls script engine first. 
		4) Client disconnects from server, starts rendering HTML

	URL: Uniform Resource Locator

	EX.1 URL and DNS resolution

		scheme://host[:port#]/path/.../[?query-string][#anchor]
		scheme 		// assign underlying protocol (such as HTTP, HTTPS, FTP)
		host		// IP or domain name of HTTP server
		port#		// default port is 80, and it can be omitted in this case. 
					// if you want to use other ports you must specify which port to use
						// http://www.example.com:8080/
		path		// resources path
		query-string	// data are sent to server
		ancho		// anchor 

	EX.1-A
		- <Recursive query process> means that the enquirers change in the process. 
		  Enquirers do not change in <Iterative query> process. 

***HTTP request package (browser information)***

	- Request packages all have three parts: request line, request header, and body. 
	  There is one blank line between header and body. 

	EX.2 HTTP Request

		GET /domains/example/HTTP/1.1		// request line: request method, URL,
											// protocol and its version
		Host: www.example.org				// domain name
		User-Agent: Mozilla/5.0 AppleWebKit/537.4 (KHTML, like Gecko) 
		Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
			// Accept: mime that clients can accept
		Accept-Encoding: gzip,deflate,sdch	// stream compression
		Accept-Charset: UTF-8,*;q=0.5		// character set in client side
		// blank line
		// body, request resource arguements (for example, arguments in POST) 

***HTTP respone package (server information)***

	EX.3 

		HTTP/1.1 200 ok				// status line
		Server: nginx/1.0.8			// web server software and its version on server
		Date:Date: Tue, 30 OCT-17	// responded time
		Content-Type: text/html		// responded data type
		Transfer-Encoding: chunked	// its means data was sent in fragments
		Connection: keep-alive		// keep connection
		Content-Length: 90			// length of body
		// blank line
		<code body> 				// message body

	EX.3-A
		- The first line is called the status line. It supplies the HTTP version, 
		  status code and status message. Which informs the client of the status of the
		  servers response. 
	
	Possible Status Codes: 
		- 1xx Informational
		- 2xx Success
		- 3xx Redirection
		- 4xx Client Error
		- 5xx Server Error

	- Keep-Alive: is used by default. If clients have additional requests, they will use
	  the same connection for them. Keep-Alive connect isn't maintained forever. The
	  server determines the limit, with which to keep the connection alive, and in most
	  cases you can configure this limit. 

	- Reducing HTTP request times is one way of improving the loading speed of web 
	  pages. By reducing the number of CSS and JS files that need to be loaded, both
	  request latencies and pressure on your web servers can be reduced at the same
	  time. 

